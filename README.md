# Laravel Scout Elasticsearch7.0 Driver

[![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](LICENSE.md)

这是在 [tamayo/laravel-scout-elastic](https://github.com/ErickTamayo/laravel-scout-elastic) 的基础上开发的适用于ElasticSearch7.0的laravel scout驱动

由于ElasticSearch7.0 不支持type 所以修改了原来的驱动 改变原来的type为统一的_doc 把searchableAs返回的字符串数据当做一个index 每个表都可以设置不同的index实现多index; 如果searchableAs 返回null 则使用scout配置中的index存储该模型数据
也可以改变MANY_MODEL的值来支持多个模型数据存储到一个index

增加了对elasticsearch  nested 类型的搜索 只需在模型中重写getNested方法返回一个nested搜索时所需的path数组则可以完成对nested类型的搜索

增加了搜索数据恢复模型后的排序

#### 使用环境：

    laravel 7.0
    laravel-scout 7.0
    elasticsearch 7.0
    elasticsearch(PHP) 5.0

## Contents

- [安装](#安装)
- [用法](#用法)
- [Credits](#credits)
- [License](#license)

## 安装

You can install the package via composer:

``` bash
composer require lf/laravel-scout-elasticsearch
```


### 在Scout配置文件中配置ElasticSearch
 在使用之前必须保证ElasticSearch已经启动并且已经创建好相关的index
```php
// config/scout.php
// Set your driver to elasticsearch
    'driver' => env('SCOUT_DRIVER', 'ElasticSearch'),

...
    'elasticsearch' => [
        'index' => env('ELASTICSEARCH_INDEX', 'laravel'),
        'hosts' => [
            env('ELASTICSEARCH_HOST', 'http://localhost'),
        ],
    ],
...
```

## 用法
模型中
```php
use ScoutEngines\ElasticSearchEngine\Search;
use Search
```
搜索用法请参考laravel官方scout的用法

## Credits

- [Erick Tamayo](https://github.com/ericktamayo)
- [All Contributors](../../contributors)

## License

The MIT License (MIT).
