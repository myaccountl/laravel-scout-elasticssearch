<?php

namespace ScoutEngines\ElasticSearchEngine;

use Laravel\Scout\Builder;
use Laravel\Scout\Searchable;

trait Search
{
    //
    use Searchable;
    /**
     * 多模型控制如果需要多个模型存储到一个索引则在类的模型中设置为true
     */
//    protected $MANY_MODEL = false;

    /**
     * 增加对多模型共同存储到一个index的支持
     * @return mixed|string
     */
    public function getSearchKey()
    {
        if($this->MANY_MODEL??false){
            return static::class . '.' . $this->getKey();
        }
        return $this->getKey();
    }

    /**
     * 通过查询的数据ID恢复成功laravel的数据模型类
     * @param Builder $builder
     * @param array $ids
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|mixed|null
     */
    public function getScoutModelsByIds(Builder $builder, array $ids)
    {
        if($this->MANY_MODEL??false){
            return $this->getScoutManyModelByIds($builder, $ids);
        }
        $new_ids = array_map(function ($q){
            return str_replace(static::class . '.', '', $q);
        }, $ids);
        $query = static::usesSoftDelete()
            ? $this->withTrashed() : $this->newQuery();

        if ($builder->queryCallback) {
            call_user_func($builder->queryCallback, $query);
        }

        $tmp = $query->whereIn(
            $this->getScoutKeyName(), $new_ids
        )->get();
        return $this->sort($builder, $tmp);
    }

    /**
     * 这是对多模型存储到一个索引共同搜索时对搜索数据进行对应的模型还原
     * @param Builder $builder
     * @param array $ids
     * @return mixed|null
     */
    public function getScoutManyModelByIds(Builder $builder, array $ids)
    {
        $array = [];
        foreach ($ids as $id){
            [$model, $_id] = explode('.', $id);
            if(!isset($array[$model])){
                $array[$model] = [];
            }
            $array[$model][] = $_id;
        }
        $tmp = collect();
        foreach ($array as $select_model => $_ids){
            if(empty($select_model)){
                continue;
            }
            $m = new $select_model;
            $query = $select_model::usesSoftDelete()
                ? $m->withTrashed() : $m->newQuery();

            if ($builder->queryCallback) {
                call_user_func($builder->queryCallback, $query);
            }
            $tmp = $tmp->merge(
                $query->whereIn(
                    $m->getScoutKeyName(), $_ids
                )->get()
            );
        }
        return $this->sort($builder, $tmp);
    }

    /**
     * 对返回的多模型集合进行排序排序字段必须所有模型都有
     * @param Builder $builder 搜索条件构造器
     * @param mixed $collect 集合
     * @return |null
     */
    protected function sort($builder, $collect)
    {
        if (count($builder->orders) == 0) {
            return $collect;
        }
        collect($builder->orders)->map(function($order) use (&$collect) {
            if(strtolower($order['direction']) == 'asc'){
                $collect = $collect->sortBy($order['column']);
            }elseif(strtolower($order['direction']) == 'desc'){
                $collect = $collect->sortByDesc($order['column']);
            }
        });
        return $collect;
    }

    /**
     * 重写方法 对不同类型进行不同数据返回
     * @return mixed|string
     */
    public function getScoutKey()
    {
        return ($this->MANY_MODEL??false) ? static::class . '.' . parent::getKey(): parent::getKey();
    }

    /**
     * 当返回数据为null时 使用配置中的index
     * @return string|null
     */
    public function searchableAs()
    {
        return null;
    }

    /**
     * 添加软删除到序列化字段
     * @param $resource
     * @return mixed
     */
    public function toSA($resource)
    {
        return $resource->additional(['__soft_deleted'=>0]);
    }

    /**
     * 设置是否支持es的nested类型查询 如果模型需要进行nested查询则重写该方法返回nested字段数组 数组中为DSL查询的path
     * @return array|null
     */
    public function getNested()
    {
        return null;
    }

}
